// Copyright 2021 Citrix
// SPDX-License-Identifier: MIT OR Apache-2.0
// There is NO WARRANTY.

use crate::prelude::*;

pub static UPSTREAM_MAIN_BRANCH: &str = "master";

pub struct Local {
  g2: git2::Repository,
  remote_head_refname: String,
  cfg: Arc<Config>,
}

#[derive(Copy,Clone,Debug,Hash,Eq,PartialEq,Ord,PartialOrd)]
#[derive(EnumString)]
#[strum(serialize_all = "SCREAMING_SNAKE_CASE")]
pub enum CveListState {
  #[strum(disabled)] Pending,
  Reserved,
  Public,
}

#[ext(pub)]
impl XsaNum {
  fn cvelist_branch_name(self) -> String { format!("xsa-{}", self) }

  fn title_with_cves(self, cves: &mut dyn Iterator<Item=&CveName>) -> String {
    let mut m = String::new();
    write!(m, "XSA-{}", self).unwrap();
    for cve in cves { write!(m, " {}", cve).unwrap(); }
    m
  }
}

impl Local {
  #[throws(AE)]
  pub fn new(cfg: &Arc<Config>, cache: &mut cache::Cache) -> Self {

    let g2 = git2::Repository::open(&cfg.cvelist_git)
      .context("open cvelist.git repo")?;

    let remote_head_refname = format!(
      "refs/remotes/{}/{}",
      cfg.cvelist_remote_upstream,
      UPSTREAM_MAIN_BRANCH
    );

    let mut repo = Local { g2, remote_head_refname, cfg: cfg.clone() };

    let mut fetch_1 = |kind, remote_name: &'_ str, branch_pat: &'_ _| {
      let did = cache.run_git_fetch_pull(
        kind, &mut repo,
        &["fetch",
          "--prune",
          "--no-tags"],
        remote_name,
        &[branch_pat],
        &[],
      )
        .with_context(|| format!("remote={} branches={}",
                                 remote_name, branch_pat))?;
      cache.did_refresh(did);
      Ok::<_,AE>(())
    };
      
    fetch_1(RefreshKind::CveListUpstream,
            &cfg.cvelist_remote_upstream,
            UPSTREAM_MAIN_BRANCH)?;
            
    fetch_1(RefreshKind::CveListXenSec,
            &cfg.cvelist_remote_xensec,
            "*")?;

    repo
  }

  pub fn cve_path(&self, cve: &CveName) -> String {
    let mut pieces = cve.as_str().split('-').skip(1);
    let year: u32 = pieces.next().unwrap().parse().unwrap();
    let num : u32 = pieces.next().unwrap().parse().unwrap();
    format!("{}/{}xxx/{}.json", year, num/1000, cve)
  }

  #[throws(AE)]
  pub fn cve_data_raw(&self, refspec: RefSpec, cve: &CveName)
                      -> Option<serde_json::Value>
  {
    let path = self.cve_path(cve);
    (||{
      let data = match self.file(refspec, &path).context("get file")? {
        Some(x) => x,
        None => return Ok(None),
      };
      let data = serde_json::from_str(&data).context("parse as JSON")?;
      Ok::<_,AE>(Some(data))
    })().with_context(|| format!("get cvelist data for {} from {:?}",
                                 &cve, path))?
  }

  #[throws(AE)]
  pub fn cve_state(&self, refspec: RefSpec, cve: &CveName)
                   -> Option<CveListState>
  {
    let data = match self.cve_data_raw(refspec, cve)? {
      None => return None,
      Some(x) => x,
    };

    let cvelist_state = data
      .as_object()          .ok_or_else(|| anyhow!("root not object"))?
      .get("CVE_data_meta") .ok_or_else(|| anyhow!("no CVE_data_meta"))?;

    #[derive(Deserialize,Debug)]
    #[allow(non_snake_case)]
    struct M {
      ID: String,
      ASSIGNER: String,
      STATE: Option<String>,
    }

    let m: M = serde_json::from_value(cvelist_state.clone())
      .context("deserialize CVE_data_meta")?;

    if m.ID != cve.as_str() {
      throw!(anyhow!("CVE_data_meta ID is wrong! {:?}", &m));
    }

    if let Some(state) = m.STATE {
      Some(state.parse().context("parse")?)
    } else if &m.ASSIGNER == &self.cfg.committer_email {
      Some(CveListState::Pending)
    } else {
      throw!(anyhow!(
        "inexplicable CVE_data_meta (no STATE, ASSIGNER is {:?} not us {:?}: \
         {:?}", &m.ASSIGNER, &self.cfg.committer_email, &m
      ))
    }
  }

  pub fn xsa_mr_branch_refname(&self, xsa: XsaNum) -> String {
    format!("refs/remotes/{}/{}",
            &self.cfg.cvelist_remote_xensec,
            &xsa.cvelist_branch_name())
  }

  #[throws(AE)]
  pub fn remote_url(&self, remote_name: &str) -> String {
    (||{
      let remote = self.g2.find_remote(remote_name).context("find remote")?;
      let url = remote.url().ok_or_else(|| anyhow!("invalid UTF-8 in url"))?;
      Ok::<_,AE>(url.to_owned())
    })()
      .with_context(|| format!("remote {:?}", remote_name))?
  }

  #[throws(AE)]
  pub fn make_xsajson_commit(&self, xsa_git: &xsagit::Repo,
                             xsa: XsaNum, vsn: XsaVsn,
                             jpaths: &BTreeMap<CveName,String>)
                             -> git2::Oid
  {
    let base_commitid = self.g2()
      .refname_to_id(&self.remote_head_refname)
      .context("look up base commitid")?;
    let base_commit = self.g2().find_commit(base_commitid)
      .context("find base commit")?;
    let base_tree = base_commit.tree()
      .context("get ase tree")?;
    let mut builder = git2::build::TreeUpdateBuilder::new();
    
    for (cve,jpath) in jpaths {
      let json_blob = self.g2()
        .blob_path(jpath.as_ref())
        .with_context(|| jpath.to_string())
        .context("construct and write json blob")?;

      builder.upsert(
        self.cve_path(cve),
        json_blob,
        git2::FileMode::Blob,
      );
    }

    let new_tree = builder.create_updated(self.g2(), &base_tree)
      .context("build new tree")?;
    let new_tree = self.g2().find_tree(new_tree).context("find new tree")?;

    let signature = git2::Signature::now(
      &self.cfg.committer_name,
      &self.cfg.committer_email,
    ).context("make signature line")?;
    
    let xsagit_head = xsa_git.head_commitid()?;

    let message = (||{
      let mut m = xsa.title_with_cves(&mut jpaths.keys());
      write!(m, "\n\n")?;
      writeln!(m, "Xensec source data: xsa.git#xsa-{}-v{}", xsa, vsn)?;
      writeln!(m, "Xensec source infra: xsa.git#{}", xsagit_head)?;
      Ok::<_,fmt::Error>(m)
    })().unwrap();

    let new_commit = self.g2().commit(
      None, &signature, &signature, &message,
      &new_tree, &[&base_commit]
    ).context("make new commit")?;

    new_commit
  }
}

impl githelp::Repo for Local {
  fn g2(&self) -> &git2::Repository { &self.g2 }
  fn cfg(&self) -> &Config { &self.cfg }
  fn dir(&self) -> &str { &self.cfg.cvelist_git }
  fn def_refname(&self) -> &str { &self.remote_head_refname }
}
