// Copyright 2021 Citrix
// SPDX-License-Identifier: MIT OR Apache-2.0
// There is NO WARRANTY.

use crate::prelude::*;

#[derive(Debug)]
pub enum RefSpec<'r> {
  Def,
  Refname(&'r str),
  Tree(&'r git2::Tree<'r>),
}
impl Default for RefSpec<'_> { fn default() -> Self { RefSpec::Def } }

pub trait Repo {
  fn g2(&self) -> &git2::Repository;
  fn dir(&self) -> &str;
  fn cfg(&self) -> &Config;

  fn def_refname(&self) -> &str;

  #[throws(AE)]
  fn get_tree_maybe(&self, refname: &str) -> Option<git2::Tree> {
    let tree = (||{
      let oid = match self.g2().refname_to_id(refname) {
        Err(e) if e.code() == git2::ErrorCode::NotFound => return Ok(None),
        x => x.context("resolve ref")?,
      };
      let commit = self.g2().find_commit(oid).context("get commit")?;
      let tree = commit.tree().context("get tree")?;
      Ok::<_,AE>(Some(tree))
    })().with_context(|| refname.to_string())?;
    tree
  }

  #[throws(AE)]
  fn get_tree<'r>(&'r self, refspec: RefSpec<'r>) -> git2::Tree<'r> {
    let refname = match refspec {
      RefSpec::Def => self.def_refname(),
      RefSpec::Refname(n) => n,
      RefSpec::Tree(t) => return t.clone(),
    };
    self.get_tree_maybe(refname)?
      .ok_or_else(|| anyhow!("ref {:?} not found", refname))?
  }

  #[throws(AE)]
  fn file(&self, refspec: RefSpec, path: &str) -> Option<String> {
    let tree = self.get_tree(refspec)?;
    (||{
      let ent = match tree.get_path(path.as_ref()) {
        Err(e) if e.code() == git2::ErrorCode::NotFound => return Ok(None),
        x => x,
      }.context("get_path")?;
      let obj = ent.to_object(self.g2()).context("to_object")?;
      let blob = obj.into_blob()
        .map_err(|e| anyhow!("not a blob, but {:?}", &e.kind()))?;
      let r = str::from_utf8(blob.content()).context("from_utf8")?;
      Ok::<_,AE>(Some(r.to_owned()))
    })()
      .with_context(|| path.to_string())
      .context("look up path")?
  }

  #[throws(AE)]
  fn files(&self, refspec: RefSpec, path: &str) -> Vec<String> {
    let tree = self.get_tree(refspec)?;
    (||{
      let ent = tree.get_path(path.as_ref()).context("get_path")?;
      let obj = ent.to_object(self.g2()).context("to_object")?;
      let tree = obj.into_tree()
        .map_err(|e| anyhow!("not a tree, but {:?}", &e.kind()))?;
      tree.iter().map(|ent| Ok::<_,AE>(
        ent.name()
          .ok_or_else(|| anyhow!("bad entry in {}, not utf-8: {:?}",
                                 path, ent.name_bytes()))?
          .to_owned()          
      ))
        .collect::<Result<Vec<_>,_>>()
    })()
      .with_context(|| path.to_string())
      .context("enumerate dir")?
  }

  #[throws(AE)]
  fn run_git(&mut self, want: bool, doing: &str,
             args1: &[&str],
             quiet_opt: &str,
             dryrun_opt: Option<&str>,
             args2: &[String]) {
    let mut build_args: Vec<String> = vec![];
    let mut pre_show: Vec<String> = vec![];

    fn dquote(s: &str) -> String {
      if s.chars().all(
        |c| c.is_ascii_alphanumeric() || "_-+=:.,/".contains(c)
      ) {
        s.to_string()
      } else {
        let mut o = "'".to_string();
        for c in s.chars() { match c {
          '\\' => o += r#"\\"#,
          '\'' => o += r#"'\''"#,
          c => o.push(c),
        } }
        o += "'";
        o
      }
    }
    
    build_args.push(self.cfg().git_command() .into());

    let our_stderr = unsafe {
      use std::os::unix::io::{AsRawFd, FromRawFd};
      let fd = nix::unistd::dup(io::stderr().as_raw_fd())
        .context("dup2 stderr for git")?;
      File::from_raw_fd(fd)
    };

    let mut cmd = Command::new(build_args[0].clone());

    cmd.stdout(our_stderr);
    cmd.current_dir(self.dir());
    pre_show.push(format!("cd {} &&", dquote(self.dir())));

    if let Some(v) = &self.cfg().git_ssh_command {
      let var = "GIT_SSH_COMMAND";
      cmd.env(var, v);
      pre_show.push(format!("{}={}", var, dquote(v)));
    }

    build_args.extend(args1.clone_strings());
    if ! log_enabled!(log::Level::Debug) {
      build_args.push(quiet_opt.to_owned());
    }
    let want =
      if want {
        true
      } else if let Some(dryrun_opt) = dryrun_opt {
        build_args.push(dryrun_opt.into());
        true
      } else {
        false
      };
    build_args.extend(args2.clone_strings());

    cmd.args(&build_args[1..]);

    let mut show = (if want { "+" } else { "#" }).to_string();
    for a in &pre_show { write!(show, " {}", a).unwrap(); }
    for a in &build_args { write!(show, " {}",  dquote(a)).unwrap(); }
    debug!("{} {}", &doing, &show);

    if want {
      let st = cmd.status().context("spawn git")?;
      if ! st.success() { throw!(
        anyhow!("git failed: {}", st)
          .context(show.clone())
          .context(self.dir().to_owned())
      ) }
    }
  }
}

impl cache::Cache {
  #[throws(AE)]
  pub fn run_git_fetch_pull(&mut self, refresh: RefreshKind,
                            repo: &mut dyn Repo,
                            args1: &[&str],
                            remote_name: &str,
                            branch_pats: &[&str],
                            extra_refspecs: &[&str])
                            -> Option<RefreshingToken>
  {
    let want = self.want_refresh(refresh, None);

    let mut args2 = vec![];
    args2.push(remote_name.to_owned());
    for pat in branch_pats {
      args2.push(format!(
        "+refs/heads/{pat}:refs/remotes/{remote}/{pat}",
        pat=pat, remote=remote_name,
      ));
    }
  
    args2.extend(extra_refspecs .iter().map(|s| s.to_string()));

    repo.run_git(want.is_some(), "updating",
                 args1, "--quiet", None, &args2)
      .context("update from remote git repo")?;

    want
  }
}

#[ext(pub)]
impl<T> &[T] where T: AsRef<str> {
  fn clone_strings(&self) -> Vec<String> {
    self.iter().map(|s| s.as_ref().to_owned()).collect_vec()
  }
}
