// Copyright 2021 Citrix
// SPDX-License-Identifier: MIT OR Apache-2.0
// There is NO WARRANTY.

use crate::prelude::*;

pub static MAIN_BRANCH: &str = "master";
pub static MAIN_REMOTE: &str = "origin";

pub type XsaNum = u32;
pub type XsaVsn = u32;

pub struct Repo {
  g2: git2::Repository,
  cfg: Arc<Config>,
}

pub type Tagged = BTreeMap<XsaNum, XsaVsn>;

#[derive(Debug,Clone,Serialize,Deserialize)]
pub struct Parsed {
  pub cves: Vec<CveName>,
  pub public: bool,
}

impl Repo {
  #[throws(AE)]
  pub fn new(cfg: &Arc<Config>, cache: &mut cache::Cache) -> Self {
    let g2 = git2::Repository::open(&cfg.xsa_git)
      .context("open xsa.git repo")?;

    let lockfile = format!("{}/.git/xensec.lock", &cfg.xsa_git);
    (||{
      let lock = File::create(&lockfile).context("open/create")?;
      lock.try_lock_exclusive().context("lock")?;
      Box::leak(Box::new(lock));
      Ok::<_,AE>(())
    })()
     .with_context(|| lockfile.clone())
     .context("acquire xensec tools mutual exclusion lock")?;

    let mut repo = Repo { g2, cfg: cfg.clone() };

    let did1 = cache.run_git_fetch_pull(
      RefreshKind::XsaGit, &mut repo,
      &["fetch",
        "--prune"],
      MAIN_REMOTE,
      &[MAIN_BRANCH],
      &[&{ let t="refs/tags/*"; format!("{}:{}",t,t) }],
    )
      .context("fetch xsa.git")?;

    let did2 = cache.run_git_fetch_pull(
      RefreshKind::XsaGit, &mut repo,
      &["merge",
        "--ff-only"],
      &format!("refs/remotes/{}/{}", MAIN_REMOTE, MAIN_BRANCH),
      &[],
      &[],
    )
      .context("update xsa.git")?;

    assert_eq!(did1,did2);
    cache.did_refresh(did1);

    repo
  }

  #[throws(AE)]
  pub fn head_commitid(&self) -> String {
    self.g2().refname_to_id("HEAD")
      .context("look up HEAD")?
      .to_string()
  }

  #[throws(AE)]
  pub fn list_by_tags(&self) -> Tagged {
    let tags = self.g2
      .tag_names(Some("xsa-*")).context("list tagw")?;
    let mut xsas = tags.into_iter()
      .filter_map(|maybe_tagname| {
        let mut pieces = maybe_tagname?.splitn(3,'-');
        assert_eq!( pieces.next(), Some("xsa"));
        let xsa = pieces.next()?.parse().ok()?;
        let vsn = pieces.next()?.strip_prefix("v")?.parse().ok()?;
        Some((xsa,vsn))
      }).collect_vec();
    xsas.sort();
    xsas.into_iter().group_by(|t| t.0)
      .into_iter().map(|(_,vsns)| vsns.max().unwrap())
      .collect()
  }

  #[throws(AE)]
  pub fn parse_advisory(&self, xsa: XsaNum, vsn: XsaVsn) -> Parsed {
    (||{
      // -E is correct because we are parsing approved, tagged, data.
      // It might not be up to modern standards.
      let std::process::Output { status, stdout, stderr:_ } = Command::new
        ("Infra/parse-advisory")
        .args(&[
          format!("-Ev{}", vsn),
          xsa.to_string(),
        ])
        .current_dir(&self.cfg.xsa_git)
        .output().context("spawn")?;

      if ! status.success() {
        throw!(anyhow!("failed, wait status {:?}", status));
      }
      let json = serde_json::from_slice(&stdout)
        .context("parse as JSON")?;

      #[derive(Debug,Deserialize)]
      #[allow(non_snake_case)]
      struct Extract {
        CVE: Option<String>,
        EmbargoTime: serde_json::Value,
        Withdrawn: Option<u8>,
      }

      let extract: Extract = serde_json::from_value(json)
        .context("interpret JSON")?;

      let cves: Vec<CveName> = match extract.CVE.as_deref() {
        None | Some("") => vec![],
        Some(s) => s
          .split(',')
          .map(|s| s.parse())
          .collect::<Result<Vec<_>,_>>()
          .context("parse CVE list")?,
      };

      let public = match &extract.EmbargoTime {
        serde_json::Value::Null => true,
        serde_json::Value::String(_) => false,
        x => throw!(anyhow!("unexpected EmbargoTime {:?}", &x)),
      };
      let _withdrawn = extract.Withdrawn.unwrap_or_default() != 0;
      // Not sure what to do about withdrawn, let's just ignore it for now

      Ok::<_,AE>(Parsed { cves, public })
    })().with_context(|| format!("parse XSA-{} v{}", xsa, vsn))?
  }

  #[throws(AE)]
  /// Leaves the JSON in xsa.git, wherefrom someone ought to read and delete
  pub fn generate_cve_json(&self, xsa: XsaNum, xstate: &analysis::XsaState)
                           -> BTreeMap<CveName, String /* abs path */>
  {
    (||{
      let jpaths = xstate.cves.keys().map(
        |cve| (cve.clone(), format!("{}/{}.json", &self.cfg.xsa_git, cve))
      ).collect::<BTreeMap<_,_>>();

      for f in jpaths.values() {
        match fs::remove_file(f) {
          Err(e) if e.kind() == ErrorKind::NotFound => { },
          x => x.with_context(|| f.to_owned()).context("remove, prep")?,
        }
      }

      let st = Command::new("Infra/parse-advisory")
        .args(&[
          "-ECC",
          &format!("-v{}", xstate.vsn),
          &xsa.to_string(),
        ])
        .current_dir(&self.cfg.xsa_git)
        .stdout(std::process::Stdio::null())
        .status().context("run parse-advisory")?;
      if ! st.success() {
        throw!(anyhow!("parse-advisory -CC failed: {:?}", st))
      }

      for f in jpaths.values() {
        fs::metadata(f).with_context(|| f.to_owned()).context("check")?;
      }

      Ok::<_,AE>(jpaths)
    })()
      .with_context(|| format!("generate CVE JSON for XSA-{}-v{}",
                               xsa, xstate.vsn))?
  }

}

impl githelp::Repo for Repo {
  fn g2(&self) -> &git2::Repository { &self.g2 }
  fn cfg(&self) -> &Config { &self.cfg }
  fn dir(&self) -> &str { &self.cfg.xsa_git }
  fn def_refname(&self) -> &str { "HEAD" }
}
