// Copyright 2021 Citrix
// SPDX-License-Identifier: MIT OR Apache-2.0
// There is NO WARRANTY.

pub use crate::prelude::*;

#[derive(Debug,Clone,StructOpt)]
pub struct CommonOpts {
  /// default log level (verbosity)
  #[structopt(long="log", short="L")]
  pub log_level: Option<log::LevelFilter>,

  /// skip `git-pull` and `git-fetch` operations
  #[structopt(long)]
  pub no_pull: bool,

  /// Do not make any remote changes (local updates still done).
  #[structopt(long)]
  pub dry_run: bool,

  /// force refresh from xsa.git and cvelist upstream, disregarding caches
  #[structopt(long="refresh")]
  pub refresh_cmdline: bool,
}

#[derive(Debug,Clone)]
pub struct Config {
  pub co: CommonOpts,
  pub done_cache:              String,
  pub git_ssh_command:         Option<String>,
  pub xsa_git:                 String,
  pub cvelist_git:             String,
  pub cvelist_remote_upstream: String,
  pub cvelist_remote_xensec:   String,
  pub cvelist_upstream:        upstream::forge::Config,
  pub committer_name:          String,
  pub committer_email:         String,
  pub start_time:              std::time::SystemTime,
}

#[derive(Debug,Clone,Default)]
#[derive(StructOpt)]
#[derive(Deserialize)]
#[derive(merge::Merge)]
pub struct ConfigSpec {
  /// sqlite3 db for cacheing work already done
  #[structopt(long, name="DB.SQLITE3")]
  pub done_cache: Option<String>,

  /// path to xsa.git working tree
  #[structopt(long, name="XSA.GIT")]
  pub xsa_git: Option<String>,

  /// path to cvelist.git tree (can be bare)
  #[structopt(long, name="CVELIST.GIT")]
  pub cvelist_git: Option<String>,

  /// remote name for cvelist.git upstream
  #[structopt(long, name="REMOTE")]
  pub cvelist_remote_upstream: Option<String>,

  /// remote name for cvelist.git upstream
  // 'Non-unique argument name: REMOTE is already in use' wat
  #[structopt(long, name="REMOTE(XENSEC)")]
  pub cvelist_remote_xensec: Option<String>,

  /// Path to cvelist upstream forge access token
  #[structopt(long, name="TOKEN/FILE")]
  pub cvelist_upstream_token: Option<PathBuf>,

  /// Value to set for GIT_SSH_COMMAND environment variable
  #[structopt(skip)]
  pub git_ssh_command: Option<String>,

  /// Name to put in git commits in the cvelist git history
  #[structopt(skip)]
  pub committer_name: Option<String>,

  /// Email address to put in git commits in the cvelist git history
  #[structopt(skip)]
  pub committer_email: Option<String>,

  /// Upstream forge details
  #[serde(default)]
  #[structopt(skip)]
  #[merge(skip)]
  pub cvelist_upstream: upstream::forge::Config,
}

impl CommonOpts {
  #[throws(AE)]
  pub fn log_init(&self) {
    let mut log_builder = env_logger::Builder::from_env("XENCNA_LOG");
    if let Some(filter) = self.log_level { log_builder.filter_level(filter); }
    log_builder.init();
  }
}

impl ConfigSpec {
  #[throws(AE)]
  pub fn resolve(self, co: &CommonOpts) -> Config {
    let config::ConfigSpec {
      xsa_git, done_cache, git_ssh_command,
      cvelist_git, cvelist_remote_upstream, cvelist_remote_xensec,
      cvelist_upstream_token, mut cvelist_upstream,
      committer_name, committer_email,
    } = self;

    macro_rules! defstr { { $v:ident = $def:expr } => {
      let $v = $v.unwrap_or($def .into());
    } }

    defstr!{ xsa_git                 = "../xsa"             }
    defstr!{ done_cache              = "cache.sqlite3"      }
    defstr!{ cvelist_git             = "../cvelist"         }
    defstr!{ cvelist_remote_upstream = "origin"             }
    defstr!{ cvelist_remote_xensec   = "xensec-github"      }
    defstr!{ committer_name          = "Xen Project Security Team" }
    defstr!{ committer_email         = "security@xenproject.org"   }

    if let Some(path) = cvelist_upstream_token {
      cvelist_upstream.token = Some(upstream::forge::TokenConfig::Path(path));
    }
    Upstream::config_resolve(&mut cvelist_upstream)?;

    let start_time = std::time::SystemTime::now();

    Config {
      co: co.clone(),
      xsa_git, done_cache, git_ssh_command,
      cvelist_git, cvelist_remote_upstream, cvelist_remote_xensec,
      cvelist_upstream,
      committer_name, committer_email, start_time,
    }
  }
}

impl Config {
  pub fn git_command(&self) -> &str { "git" }
}

pub struct Ctx {
  pub cfg: Arc<Config>,
  pub xsas: xsagit::Repo,
  pub cvelist: cvelist::Local,
  pub cache: cache::Cache,
  pub upstream: Upstream,
}

impl Ctx {
  #[throws(AE)]
  pub fn new(cfg: Config) -> Self {
    let cfg = Arc::new(cfg);
    let mut cache = cache::Cache::new(&cfg)?;
    let xsas = xsagit::Repo::new(&cfg, &mut cache)?;
    let cvelist = cvelist::Local::new(&cfg, &mut cache)?;
    let upstream = Upstream::new(&cfg, &cvelist)?;
    // xxx pull xsas, cves
    Ctx { cfg, xsas, cache, cvelist, upstream }
  }
}
