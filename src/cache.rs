// Copyright 2021 Citrix
// SPDX-License-Identifier: MIT OR Apache-2.0
// There is NO WARRANTY.

use crate::prelude::*;

use rusqlite::{/*params,*/ Connection, Transaction, ToSql, types::ValueRef};

type RE = rusqlite::Error;

const MAX_RETRIES: u32 = 10;

#[derive(Debug,Copy,Clone)]
#[derive(Hash,Eq,PartialEq,Ord,PartialOrd)]
#[derive(EnumIter)]
pub enum RefreshKind {
  XsaGit,
  XsaParsed,
  CveListXenSec,
  CveListUpstream,
  UpstreamMr(XsaNum),
}

type Refreshed = BTreeSet<Refresh>;

#[derive(Debug,Copy,Clone)]
#[derive(Hash,Eq,PartialEq,Ord,PartialOrd)]
pub struct Refresh(RefreshKind /* XsaNum is actually set */);

#[derive(Debug)]
pub struct Cache {
  pub cfg: Arc<Config>,
  pub refreshed: Refreshed,
  pub do_refresh: bool,
  db: Connection,
}

impl RefreshKind {
  pub fn bind(self, xsa: Option<XsaNum>) -> Refresh {
    use RefreshKind as R;
    Refresh(match self {
      R::UpstreamMr(old) => { assert_eq!(old,0); R::UpstreamMr(xsa.unwrap()) },
      other => other,
    })
  }
}

#[derive(Debug,Copy,Clone)]
#[derive(Hash,Eq,PartialEq,Ord,PartialOrd)]
pub struct RefreshingToken(Refresh);

impl Cache {
  pub fn want_refresh(&self, rk: RefreshKind, xsa: Option<XsaNum>)
                      -> Option<RefreshingToken>
  {
    use RefreshKind as R;
    let want = match rk {
      R::XsaGit |
      R::CveListXenSec |
      R::CveListUpstream => ! self.cfg.co.no_pull,
      R::XsaParsed |
      R::UpstreamMr(_) => self.do_refresh,
    };
    if want {
      Some(RefreshingToken(rk.bind(xsa)))
    } else {
      None
    }
  }
}

impl Ctx {
  #[throws(AE)]
  pub fn want_remote_actions(&self, xsa: XsaNum,
                             desc: &str, info: &dyn Debug)
                             -> bool
  {
    use RefreshKind as R;
    let want = ! self.cfg.co.dry_run;

    if want && self.cfg.co.no_pull { throw!(anyhow!(
      "--no-pull specified with action which makes remote updats"
    )); }

    for rf in RefreshKind::iter() {
      let rf = rf.bind(Some(xsa));
      match rf {
        Refresh(R::XsaParsed) => {
          // invalidation is automatic based on version
        },
        rf => {
          if ! self.cache.refreshed.contains(&rf) {
            let trouble = format!(
              "program logic error, did not refresh {:?}, did {:?}",
              rf, &self.cache.refreshed
            );
            if want {
              panic!("{}", trouble);
            } else {
              eprintln!("WWOULD FAIL BECAUSE {}", trouble);
            }
          }
        }
      }
    }

    if ! want {
      eprintln!("dry run, would {} {:?}", desc, info);
    }

    want
  }
} 

impl Cache {
  pub fn did_refresh(&mut self, rt: Option<RefreshingToken>) {
    if let Some(rt) = rt {
      self.refreshed.insert(rt.0);
    }
  }
}

static SCHEMA: &[&str] = &[r#"
        DROP TABLE IF EXISTS xsagit_parsed_1;
"#,r#"
        CREATE TABLE IF NOT EXISTS xsagit_parsed_2 (
            xsa  INTEGER PRIMARY KEY,
            vsn  INTEGER NOT NULL,
            json TEXT    NOT NULL
        );
"#,r#"
        CREATE TABLE IF NOT EXISTS cvelist_mr_1 (
            xsa  INTEGER PRIMARY KEY,
            json TEXT    NOT NULL
        );            
"#];

pub trait Cacheable: Serialize + DeserializeOwned + Debug {
  type Key: Debug;
  const TABLE: &'static str;
  const KEY_KCOLS: &'static [&'static str];
  const KEY_VCOLS: &'static [&'static str];
  const REFRESH: RefreshKind;
  fn key_col_values(key: &Self::Key) -> Result<Vec<&dyn ToSql>, AE>;
}

impl Cacheable for xsagit::Parsed {
  // Invalidation:
  //
  // Cache will automatically be invalidated by new xsa publication,
  // because Vsn is in KEY_VCOLS.
  //
  // If the xsa.git code changes in ways which change the generated
  // Parsed, this won't be detected and a manual invalidation will be
  // needed.
  type Key = (XsaNum, XsaVsn);
  const TABLE: &'static str = "xsagit_parsed_2";
  const KEY_KCOLS: &'static [&'static str] = &["xsa"];
  const KEY_VCOLS: &'static [&'static str] = &["vsn"];
  const REFRESH: RefreshKind = RefreshKind::XsaParsed;
  #[throws(AE)]
  fn key_col_values<'k>((xsa, vsn): &'k (XsaNum, XsaVsn))  -> Vec<&'k dyn ToSql>
  { vec![xsa as _, vsn as _] }
}

impl Cacheable for upstream::MrState {
  // Invalidation:
  //
  // Merge requets are supposed to be created only by us.  If they
  // are created manually, the cache must be manually invalidated.
  //
  // When (before) we create an MR, code here invalidates the cache
  // and recheck the status.
  //
  // If an MR is merged this will show up as the CVEs being Populated
  // which likewise means we can invalidate the cache.
  //
  // If an MR is closed, we don't detect this.  We will have a cron
  // job to refresh the status.
  type Key = XsaNum;
  const TABLE: &'static str = "cvelist_mr_1";
  const KEY_KCOLS: &'static [&'static str] = &["xsa"];
  const KEY_VCOLS: &'static [&'static str] = &[];
  const REFRESH: RefreshKind = RefreshKind::UpstreamMr(0);
  #[throws(AE)]
  fn key_col_values<'k>(xsa: &'k XsaNum)  -> Vec<&'k dyn ToSql>
  { vec![xsa as _] }
}

#[throws(fmt::Error)]
fn write_cond_where_cols<'k,S,C>(mut stmt: S, cols: C)
where S: fmt::Write,
      C: Iterator<Item=&'k str>
{
  for (k, conj) in izip!(
    cols,
    iter::once("WHERE").chain(iter::repeat("AND")),
  ) {
    write!(stmt, " {} {}=?", conj, k)?;
  }
}

impl Cache {
  #[throws(AE)]
  pub fn new(cfg: &Arc<Config>) -> Self {
    let db = rusqlite::Connection::open(&cfg.done_cache)
      .with_context(|| cfg.done_cache.clone()).context("opne db")?;

    for stmt in SCHEMA {
      db.execute(stmt,[])
        .with_context(|| stmt.to_owned())
        .context("ensure schema is set up")?;
    }

    Cache {
      db,
      cfg: cfg.clone(),
      refreshed: default(),
      do_refresh: cfg.co.refresh_cmdline,
    }
  }

  #[throws(AE)]
  pub fn with<F,O>(&mut self, mut f: F) -> O
  where F: FnMut(&mut Transaction) -> Result<O, AE>
  {
    use rusqlite::ffi::ErrorCode as E;
    'ret: loop { for retry in 0.. { match (||{
      let mut trans = self.db.transaction().context("start transaction")?;
      let r = f(&mut trans)?;
      trans.commit().context("commit")?;
      Ok::<_,AE>(r)
    })() {
      Err(e) => match e.downcast_ref() {
        Some(RE::SqliteFailure(ffie, _))
          if matches!(ffie.code, E::DatabaseBusy | E::DatabaseLocked)
          => {
            if retry >= MAX_RETRIES {
              throw!(e.context("too many retries"));
            }
            continue;
          },
        _ => throw!(e),
      }
      Ok(r) => break 'ret r,
    } } }
  }

  #[throws(AE)]
  pub fn invalidate<T>(&self, key: &T::Key)
  where T: Cacheable,
  {
    let keys = T::key_col_values(key).context("get keys as params")?;
    let keys = &keys[0.. T::KEY_KCOLS.len()];
    let stmt = (||{
      let mut stmt = String::new();
      write!(stmt, "DELETE FROM {}", T::TABLE)?;
      write_cond_where_cols(&mut stmt, T::KEY_KCOLS.iter().cloned())?;
      Ok::<_,fmt::Error>(stmt)
    })().unwrap();

    (||{
      let mut stmt = self.db.prepare_cached(&stmt).context("prepare")?;
      stmt.execute(&*keys).context("execute delete")?;
      Ok::<_,AE>(())
    })()
      .with_context(|| format!("key={:?}", key))
      .context("invalidate cache line")?;
  }

  #[throws(AE)]
  pub fn lookup<T,F>(&mut self, xsa: XsaNum, key: &T::Key, mut calc: F) -> T
  where T: Cacheable,
        F: FnMut(&T::Key) -> Result<T,AE>
  {
    let all_cols = || itertools::chain(T::KEY_KCOLS, T::KEY_VCOLS).cloned();

    let stmt = (||{
      let mut stmt = String::new();
      write!(stmt, "SELECT json FROM {}", T::TABLE)?;
      write_cond_where_cols(&mut stmt, &mut all_cols())?;
      Ok::<_,fmt::Error>(stmt)
    })().unwrap();

    let got = (||{
      let keys = T::key_col_values(key).context("get keys as params")?;
      assert_eq!( keys.len(), all_cols().count(), "{:?}", key );

      let (data, refresh_token) = (||{
        let refresh_token = self.want_refresh(T::REFRESH, Some(xsa));
        if refresh_token.is_some() { return Ok((None, refresh_token)); }

        let mut stmt = self.db.prepare_cached(&stmt).context("prepare")?;
        let mut data = stmt.query(keys.as_slice()).context("query")?;
        let data = data.next().context("get query row")?;
        let data = data.map(|row| {
          let data = row.get_ref(0).context("get result json column")?;
          let data = match data {
            ValueRef::Text(s) => s,
            x => throw!(anyhow!("got wrong type, expected Text {:?}", &x)),
          };
          let data = str::from_utf8(data).context("got non-UTF-8 json")?;
          let data = serde_json::from_str(data).context("parse JSON")?;
          Ok::<T,AE>(data)
        })
          .transpose()?;
        Ok::<(Option<T>, Option<RefreshingToken>),AE>((data, refresh_token))
      })()
        .with_context(|| stmt.clone())
        .context("look up in cache")?;

      let hit = data.is_some();
      let data = data.map(Ok).unwrap_or_else(|| {
        let data = calc(key).context("calculate, after cache miss")?;

        let stmt = (||{
          let mut stmt = String::new();
          write!(stmt, "INSERT OR REPLACE INTO {} (", T::TABLE)?;
          for k in all_cols() { write!(stmt, "{},", k)?; }
          write!(stmt, "json) VALUES (")?;
          for _ in all_cols() { write!(stmt, "?,")?; }
          write!(stmt, "?)")?;
          Ok::<_,fmt::Error>(stmt)
        })().unwrap();

        (||{
          let json = serde_json::to_string(&data).context("make JSON")?;
          let mut stmt = self.db.prepare_cached(&stmt).context("prepare")?;
          let mut keys = keys;
          keys.push(&json);
          stmt.execute(&*keys).context("execute insert")?;
          Ok::<_,AE>(())
        })()
          .with_context(|| stmt.clone())
          .context("save in cache")?;

        self.did_refresh(refresh_token);

        Ok::<T,AE>(data)
      })?;
      Ok::<_,AE>((data, hit))
    })();

    trace!("lookup {} key={:?} {} got={:?}",
           T::TABLE, key,
           match got {
             Err(_)        => "error",
             Ok((_,false)) => "miss",
             Ok((_,true )) => "hit",
           },
           &got);

    got
      .with_context(|| format!("key={:?}", key))
      .context("lookup in cache db")?
      .0
  }
}
