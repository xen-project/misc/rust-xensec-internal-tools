// Copyright 2021 Citrix
// SPDX-License-Identifier: MIT OR Apache-2.0
// There is NO WARRANTY.

use crate::prelude::*;

#[derive(Debug,Clone,Eq,PartialEq,Hash)]
#[derive(derive_more::Display)]
#[derive(Serialize,Deserialize)]
#[serde(transparent)]
pub struct CveName(String);

#[derive(Debug,Clone)]
pub struct Cves {
  pub cves: BTreeMap<CveName, CveState>,
}

pub struct Global {
  pub tagged: xsagit::Tagged,
  pub cves: analysis::Cves,
  pub xsas: GlobalXsas,
}
type GlobalXsas = BTreeMap<XsaNum, XsaState>;

#[derive(Debug,Clone,EnumString)]
#[derive(Hash,Eq,PartialEq,Ord,PartialOrd)]
pub enum CveState {
  Foreign,
  Available,
  Allocated(XsaNum),
  Populated(XsaNum),
  Published(XsaNum),
  Broken(String),
}
impl Default for CveState { fn default() -> Self { Self::Foreign } }

#[derive(Debug,Clone)]
pub struct XsaState {
  pub vsn: XsaVsn,
  pub cves: BTreeMap<CveName, CveInMrState>,
  pub public: bool,
  pub remote_branch: bool,
  pub merge_req: Option<upstream::MrState>, // None: info not relevant/gathered
}

#[derive(Debug,Clone,EnumString)]
#[derive(Eq,PartialEq,Ord,PartialOrd)]
#[derive(Serialize,Deserialize)]
pub enum CveInMrState {
  Foreign,
  NoBranch,
  InMr,
  MissingFromBranch,
  Completed,
  Broken(String),
}

#[derive(Debug,Copy,Clone,EnumString)]
#[derive(Eq,PartialEq,Ord,PartialOrd)]
#[derive(Serialize,Deserialize,EnumIter)]
pub enum Todo {
  TrySubmit,
  DeleteBranch,
}

pub type Todos = BTreeMap<Todo, Vec<XsaNum>>;

impl Ord for CveName {
  fn cmp(&self, other: &Self) -> cmp::Ordering {
    let a = self .0.split('-');
    let b = other.0.split('-');
    for e in a.zip_longest(b) { match e {
      EitherOrBoth::Left(_) => return Ordering::Greater,
      EitherOrBoth::Right(_) => return Ordering::Less,
      EitherOrBoth::Both(a,b) => {
        fn n(s: &str) -> (Option<usize>, &str) { (s.parse().ok(), s) }
        let an = n(a);
        let bn = n(b);
        let c = an.cmp(&bn);
        if c != Ordering::Equal { return c };
      },
    } }
    return Ordering::Equal;
  }
}
impl PartialOrd for CveName {
  fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
    Some(self.cmp(other))
  }
}
impl FromStr for CveName {
  type Err = AE;
  #[throws(AE)]
  fn from_str(s: &str) -> Self {
    (||{
      let mut pieces = s.split('-');
      for i in 0..3 {
        let piece = pieces.next().ok_or_else(|| anyhow!("too few pieces"))?;
        if piece.is_empty() { throw!(anyhow!("consecutive/initial hyphen")) }
        if piece.len() > 6 { throw!(anyhow!("piece too long")) }
        if ! piece.chars().all(|c| {
          if i == 0 { c.is_ascii_alphabetic() }
          else { c.is_ascii_digit() }
        }) { throw!(anyhow!("bad char in piece {}", i)); }
      }
      if pieces.next().is_some() { throw!(anyhow!("too many pieces")) }
      Ok::<_,AE>(CveName(s.into()))
    })().with_context(|| format!("parse cve name {:?}", s))?
  }
}
impl CveName                { pub fn as_str(&self) -> &str { &self.0 } }
impl AsRef<str> for CveName {     fn as_ref(&self) -> &str { &self.0 } }

impl Cves {
  #[throws(AE)]
  pub fn ascertain(ctx: &Ctx) -> Self {
    let mut cves = BTreeMap::new();
    let mut insert = |cve: CveName, state: CveState| {
      if let Some(dupe) = cves.insert(cve.clone(), state.clone()) {
        throw!(anyhow!("duplicate state for {:?}: {:?} vs {:?}",
                       &cve, dupe, &state));
      }
      Ok::<_,AE>(())
    };

    (||{
      let available = ctx.xsas.file(default(), "CNA/cves-available.json")?
        .ok_or_else(|| anyhow!("missing cves-availoable"))?;
      let available: Vec<String> = serde_json::from_str(&available)?;

      for cve in available {
        let cve = cve.parse()?;
        insert(cve, CveState::Available)?;
      }
      Ok::<_,AE>(())
    })().context("process cves-available")?;

    for leaf in ctx.xsas.files(default(), "CNA/allocated")
      .context("scan CNA/allocated")?
    {
      let (cve, extn) = match leaf.split('.').collect_tuple() {
        Some(x) => x,
        None => continue,
      };
      if extn != "toml" { continue }
      let cve: CveName = match cve.parse() {
        Ok(x) => x,
        Err(_) => continue,
      };

      let path = format!("CNA/allocated/{}", &leaf);

      (||{
        let data = ctx.xsas.file(default(), &path)?
          .ok_or(anyhow!("file vanished!"))?;

        #[derive(Deserialize)]
        struct Allocd { xsa: XsaNum }

        let allocd: Allocd = toml::de::from_str(&data).context("parse TOML")?;
        insert(cve, CveState::Allocated(allocd.xsa))?;

        Ok::<_,AE>(())
      })().with_context(|| path.clone())?;
    }

    drop(insert);

    for (cve, our_state) in &mut cves {
      use CveState    as O;
      use CveListState as L;
      if matches!(our_state, O::Foreign | O::Broken(_)) { continue }

      let change_state = (||{
        let cvelist_state = ctx.cvelist.cve_state(default(), &cve)?
          .ok_or_else(|| anyhow!("cvelist data missing"))?;

        Ok::<_,AE>(match (&our_state, cvelist_state) {
          (O::Foreign       , _) => panic!(),
          (O::Broken   (_  ), _) => panic!(),
          (O::Populated(_  ), _) => panic!(),
          (O::Available     , L::Reserved) => None,
          (O::Allocated(_  ), L::Reserved) => None,
          (O::Allocated(xsa), L::Pending ) => Some(O::Populated(*xsa)),
          (O::Allocated(xsa), L::Public  ) => Some(O::Published(*xsa)),
          _ => throw!(anyhow!("unexpected states ours={:?} cvelist={:?}",
                              &our_state, &cvelist_state)),
        })
      })().with_context(|| format!("check cvelist for {}", cve));

      match change_state {
        Ok(Some(new_state)) => *our_state = new_state,
        Ok(None) => { },
        Err(e) => *our_state = O::Broken(format!("{:?}", e)),
      }
    }

    Cves { cves }
  }

  #[throws(AE)]
  pub fn ascertain_one_xsa(&mut self, ctx: &mut Ctx, tagged: &xsagit::Tagged,
                           xsa: XsaNum, force_mr_check: bool) -> XsaState
  {
    (||{ 
      let vsn = *tagged.get(&xsa).ok_or_else(|| anyhow!("not tagged"))?;

      let parsed = {
        let xsas = &ctx.xsas;
        ctx.cache.lookup(xsa, &(xsa, vsn), |&(xsa,vsn)| {
          xsas.parse_advisory(xsa, vsn)
        })?
      };

      // xxx want to fetch

      let mr_branch = ctx.cvelist.xsa_mr_branch_refname(xsa);
      let tree = ctx.cvelist.get_tree_maybe(&mr_branch)?;
      let mut cves = BTreeMap::new();

      for cve in parsed.cves { cves.insert(cve.clone(), {
        use CveState     as O;
        use CveInMrState as I;
        use CveListState as L;

        let imrs = match self.cves.entry(cve.clone()).or_default() {
          O::Allocated(x) if *x == xsa => match &tree {
            None => I::NoBranch,
            Some(tree) => match ctx.cvelist.cve_state(
              RefSpec::Tree(tree), &cve
            ) {
              Err(e) => I::Broken(format!("mr {:?} cvelist state parse: {}",
                                          &mr_branch, e)),
              Ok(Some(L::Public  )) => I::InMr,
              Ok(Some(L::Pending )) => I::InMr,
              Ok(Some(L::Reserved)) => I::MissingFromBranch,
              Ok(None             ) => I::MissingFromBranch,
            }
          },
          O::Foreign                   => I::Foreign,
          O::Populated(x) |
          O::Published(x) if *x == xsa => I::Completed,

          x@ O::Available |
          x@ O::Published(_) |
          x@ O::Populated(_) |
          x@ O::Allocated(_) |
          x@ O::Broken(_) => {
            I::Broken(format!("in advisory-{} v{} but CNA/ says {:?}",
                              &xsa, &vsn, x))
          },
        };

        imrs
      }); }

      let remote_branch = tree.is_some();
      let cve_wants_mr_state = |v| v == &CveInMrState::InMr;
      let merge_req = if
        force_mr_check || (
          remote_branch &&
          cves.values().any(cve_wants_mr_state)
        )
      {
        let upstream = &mut ctx.upstream;
        Some(ctx.cache.lookup(xsa, &xsa, |&xsa| {
          upstream.get_mr_state(xsa)
        })?)
      } else {
        ctx.cache.invalidate::<MrState>(&xsa)?;
        None
      };

      if ! remote_branch &&
         ctx.cache.refreshed.contains(&RefreshKind::CveListXenSec.bind(None))
      {
        // MR cannot exist (at least, not open) if branch does not, so
        // treat it as having been refreshed even though we didn't
        // query the forge.
        assert!(merge_req.is_none());
        ctx.cache.refreshed.insert(
          RefreshKind::UpstreamMr(default()).bind(Some(xsa))
        );
      }

      let state = XsaState {
        vsn, cves, merge_req, remote_branch,
        public: parsed.public,
      };
      Ok::<_,AE>(state)
    })().with_context(|| format!("ascertain XSA-{}", xsa))?
  }

}

impl XsaState {
  pub fn todo(&self, xsa: XsaNum, cves: &Cves) -> Option<Todo> {
    use analysis::CveState as O;
    use analysis::CveInMrState as I;

    #[derive(Debug,Copy,Clone,Eq,PartialEq,Ord,PartialOrd)]
    enum Active { Keep, TrySubmit }

    let active = self.cves.iter().filter_map(|(cve, inmr)| {
      let st = cves.cves.get(cve).cloned().unwrap_or_default();
      let subm = |why| Some((why, Active::TrySubmit));
      let active = match st {

        O::Foreign      |
        O::Available    |
        O::Published(_) => None,

        O::Populated(_) => Some((format!("{:?}",st), Active::Keep)),

        O::Broken(_) => subm(format!("{:?}", st)),

        O::Allocated(_) => {
          let mr = self.merge_req.as_ref();
          if ! matches!(inmr, I::InMr) { subm(format!("I::{:?}", inmr)) }
          else if ! self.remote_branch { subm("no branch".into()) }
          else if mr.is_none() { subm("no MR info!".into()) }
          else if mr.unwrap().state.is_none() { subm("no MR".into()) }
          else {
            let why = format!("await merge of #{}",
                              mr.unwrap().state.as_ref().unwrap().num);
            Some((why, Active::Keep))
          }
        },

      };
      match &active {
        None => {
          trace!("XSA-{} {} ok {:?}", xsa, cve, &st);
          None
        },
        Some((why, todo)) => {
          debug!("XSA-{} {} todo {} {:?}", xsa, cve, why, &st);
          Some(*todo)
        }
      }
    }).max();

    match active {
      Some(Active::TrySubmit)    => Some(Todo::TrySubmit),
      Some(Active::Keep     )    => None,
      None if self.remote_branch => Some(Todo::DeleteBranch),
      None                       => None,
    }
  }
}

impl Ctx {
  #[throws(AE)]
  pub fn ascertain_everything(&mut self) -> Global {
    let tagged = self.xsas.list_by_tags()?;
    let mut cves = analysis::Cves::ascertain(self)?;
    let mut xsas: GlobalXsas = default();

    for &xsa in tagged.keys() {
      let xstate = cves.ascertain_one_xsa(self, &tagged, xsa, false)?;
      xsas.insert(xsa, xstate);
    }
    Global {
      tagged,
      cves,
      xsas,
    }
  }

  #[throws(AE)]
  pub fn todos(&mut self) -> (Global, Todos) {
    let global = self.ascertain_everything()?;
    let mut todos: Todos = default();
    for todo in Todo::iter() { todos.insert(todo, default()); }

    for (&xsa, xstate) in &global.xsas {
      if ! xstate.public {
        trace!("XSA-{} embargoed", xsa);
        continue
      }

      let todo = match xstate.todo(xsa, &global.cves) {
        None => continue,
        Some(todo) => todo,
      };

      todos.get_mut(&todo).unwrap().push(xsa);
    }

    self.cache.do_refresh = true; // in prep for doing things...

    (global, todos)
  }
}
