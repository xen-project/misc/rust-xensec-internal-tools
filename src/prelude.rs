// Copyright 2021 Citrix
// SPDX-License-Identifier: MIT OR Apache-2.0
// There is NO WARRANTY.

pub use std::cmp::{self, max, Ordering};
pub use std::collections::{btree_map, BTreeMap};
pub use std::collections::{btree_set, BTreeSet};
pub use std::fmt::{self, Debug, Display, Write as _};
pub use std::fs::{self, File};
pub use std::iter;
pub use std::io::{self, ErrorKind};
pub use std::mem::{self, take};
pub use std::path::PathBuf;
pub use std::process::Command;
pub use std::str::{self, FromStr};
pub use std::sync::Arc;

pub use anyhow::{anyhow, Context};
pub use either::{Either, Left, Right};
pub use extend::ext;
pub use fehler::{throw, throws};
pub use fs2::FileExt;
pub use itertools::{izip, EitherOrBoth, Itertools};
pub use log::{log_enabled, debug, info, trace};
pub use serde::{Deserialize, de::DeserializeOwned, Serialize};
pub use structopt::StructOpt;
pub use strum::{Display, EnumIter, EnumString, IntoEnumIterator as _};

pub use crate::analysis::{self, CveName, Global, Todo, Todos};
pub use crate::cache::{self, RefreshKind, RefreshingToken};
pub use crate::config::{self, Config, Ctx};
pub use crate::cvelist::{self, CveListState, XsaNumExt as _};
pub use crate::githelp::{self, RefSpec, Repo as _};
pub use crate::upstream::{self, MrState, Upstream};
pub use crate::xsagit::{self, XsaNum, XsaVsn};

pub type AE = anyhow::Error;

pub fn default<T>() -> T where T: Default { Default::default() }
