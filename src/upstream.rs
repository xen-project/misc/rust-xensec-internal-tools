// Copyright 2021 Citrix
// SPDX-License-Identifier: MIT OR Apache-2.0
// There is NO WARRANTY.

use crate::prelude::*;
//use forge::*;

pub use gitforge::forge;
pub use forge::IssueMrStatus as MrStatus;

use forge::*;

const DEFAULT_HOST: &str = "github.com";
const DEFAULT_KIND: forge::Kind = forge::Kind::GitHub;

#[derive(Debug,Clone)]
#[derive(Serialize,Deserialize)]
pub struct MrState {
  pub state: Option<MrInfo>, // None: MR does not exist
}

#[derive(Debug,Clone)]
#[derive(Serialize,Deserialize)]
pub struct MrInfo {
  pub num: String,
  pub status: MrStatus,
}

#[derive(Debug)]
pub struct Upstream {
  forge: Box<dyn forge::Forge>,
  cvelist_repo_upstream: String,
  cvelist_repo_xensec:   String,
}

impl Upstream {
  #[throws(AE)]
  pub fn config_resolve(config: &mut forge::Config) {
    if config.host == "" { config.host = DEFAULT_HOST.into() }
    config.kind.get_or_insert(DEFAULT_KIND);
  }

  #[throws(AE)]
  pub fn new(cfg: &Arc<config::Config>, cvelist: &cvelist::Local) -> Self {
    let forge = cfg.cvelist_upstream.forge()?;

    let rrepo = |remote_name: &str| (||{
      let remote_url = cvelist.remote_url(remote_name)?;
      let repo = forge.remote_url_to_repo(&remote_url)?;
      Ok::<_,AE>(repo)
    })().with_context(|| remote_name.to_owned()).context("find remote repo");

    Upstream {
      cvelist_repo_upstream: rrepo(&cfg.cvelist_remote_upstream)?,
      cvelist_repo_xensec:   rrepo(&cfg.cvelist_remote_xensec  )?,
      forge,
    }
  }

  #[throws(AE)]
  pub fn get_mr_state(&mut self, xsa: XsaNum) -> MrState {
    let req = Req::MergeRequests(Req_MergeRequests {
      target_repo:         self.cvelist_repo_upstream    .clone()  ,
      source_repo:   Some( self.cvelist_repo_xensec      .clone() ),
      source_branch: Some( xsa.cvelist_branch_name()              ),
      target_branch: Some( cvelist::UPSTREAM_MAIN_BRANCH .into()  ),
      ..default()
    });
    let resp = self.forge.request(&req)?;
    let unprocessable = |s|
      anyhow!("unprocessable response: {} ", s)
      .context(format!("req={:?}", req))
      .context(format!("resp={:?}", resp));

    let mrs = match &resp {
      Resp::MergeRequests { mrs,.. } => mrs,
      _ => throw!(unprocessable("unexpected variant")),
    };
    let all = mrs.iter().collect_vec();
    let not_closed = mrs.iter()
      .filter(|mr| mr.state.status != MrStatus::Closed)
      .collect_vec();

    debug!("{} all:{:?} not-closed:{:?}", xsa, &all, &not_closed);
    let mrs = if mrs.len() > 1 && not_closed.len() == 1 {
      &not_closed
    } else {
      &all
    };

    match mrs.as_slice() {
      [] => {
        MrState { state: None }
      },

      [mr] => {
        let status = match mr.state.status {
          y@ MrStatus::Open |
          y@ MrStatus::Closed |
          y@ MrStatus::Merged => y,
          _ => throw!(unprocessable("unexpected KR status")),
        };
        MrState { state: Some(MrInfo{
          status,
          num: mr.number.clone(),
        }) }
      },

      _ => throw!(unprocessable("more than 1 MR")),
    }
  }
}

impl Ctx {
  #[throws(AE)]
  pub fn make_mr(&mut self, xsa: XsaNum, xstate: &analysis::XsaState)
                 -> String
  {
    let title = xsa.title_with_cves(&mut xstate.cves.keys());
    let description = (||{
      let mut d = String::new();
      write!(d, "{}\n\n", &title)?;
      writeln!(d, "CVE data entry for:")?;
      for cve in xstate.cves.keys() {
        writeln!(d, "  {}", &cve)?;
        write!(d, r#"
(This MR was automatically generated by the Xen Project Security Team's
xensec-internal-tools.  Please let us know if anything could be better.)
"#
        )?;
      }
      Ok::<_,AE>(d)
    })().unwrap();

    let req = Req::CreateMergeRequest(Req_CreateMergeRequest {
      target: RepoBranch {
        repo:              self.upstream.cvelist_repo_upstream   .clone()  ,
        branch:            cvelist::UPSTREAM_MAIN_BRANCH         .into()   ,
      },
      source: RepoBranch {
        repo:              self.upstream.cvelist_repo_xensec     .clone()  ,
        branch:            xsa.cvelist_branch_name()                       ,
      },
      title,
      description,
      ..default()
    });

    if self.want_remote_actions(xsa,"make mr",&req)? {
      self.cache.invalidate::<upstream::MrState>(&xsa)
        .context("clear going-to-be-stale cache")?;

      let resp = self.upstream.forge
        .request(&req).context("make merge request")?;

      let number = match &resp {
        Resp::CreateMergeRequest { number,.. } => number,
        _ => throw!(anyhow!("unexpected variant: {:?}", &resp)),
      };

      number.clone()
    } else {
      "<dry run, MR not created>".into()
    }
  }
}
