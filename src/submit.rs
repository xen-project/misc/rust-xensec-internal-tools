// Copyright 2021 Citrix
// SPDX-License-Identifier: MIT OR Apache-2.0
// There is NO WARRANTY.

use crate::prelude::*;

#[derive(Debug,Clone)]
#[derive(Ord,PartialOrd,Eq,PartialEq)]
pub enum NothingDone {
  Embargoed,
  NoCves,
  OnlyForeignCves,
  MrOpen(String),
  Completed,
  ManualActionRequired(Vec<String>),
}

use NothingDone as N;
use upstream::MrStatus as M;
use analysis::CveInMrState as I;

#[ext]
impl<T> T where T: Ord + Clone {
  fn umax(&mut self, other: T) { *self = max(self.clone(), other) }
}

#[derive(Debug,Clone)]
pub enum SubmitOutcome {
  MergeReq(String),
  No(NothingDone),
}
use SubmitOutcome as SO;
impl SubmitOutcome {
  #[throws(fmt::Error)]
  pub fn report(&self, w: &mut dyn fmt::Write, xsa: XsaNum) {
    match self {
      SO::MergeReq(number) => {
        writeln!(w, "{} SUBMITTED {:?}", xsa, number)?;
      },
      SO::No(no) => {
        no.report(w, xsa)?
      }
    }
  }
}

impl Ctx {
  #[throws(AE)]
  pub fn submit1(&mut self, xsa: XsaNum, xstate: analysis::XsaState)
                 -> SubmitOutcome
  {
    if ! xstate.public { return SO::No(NothingDone::Embargoed) }

    let mut todo: Either<NothingDone, ()> = Left(N::NoCves);
    let mut manual = vec![];

    for (cve, st) in &xstate.cves {
      match st {
        I::Foreign   => todo.umax(Left(N::OnlyForeignCves)),
        I::Completed => todo.umax(Left(N::Completed)),
        I::NoBranch  => todo = Right(()), // can't both happen;
        I::InMr      => todo = Right(()), // see XsaState.remote_branch
        I::Broken(x) => manual.push(format!("{} broken: {}", cve, x)),
        I::MissingFromBranch => manual.push(format!(
          "{} missing from (remote tracking) branch {}",
          cve, xsa.cvelist_branch_name(),
        )),
      }
    }

    if ! manual.is_empty() { return SO::No(N::ManualActionRequired(manual)) }
    match todo {
      Left(nothing) => return SO::No(nothing),
      Right(()) => { }, // carry on
    }

    #[derive(Copy,Clone)] struct ThereIsNoMr;
    let was_no_mr = match xstate.merge_req.as_ref().map(|s| &s.state) {
      None if ! xstate.remote_branch => ThereIsNoMr,
      Some(None) => ThereIsNoMr,
      Some(Some(mr)) => return SO::No(match mr.status {
        M::Open => N::MrOpen(format!(
          "#{}", mr.num
        )),
        x => N::ManualActionRequired(vec![format!(
          "MR #{} in state {:?}, CVEs not populated in upstream main branch",
          &mr.num, &x
        )]),
      }),
      None => panic!("mr not examined! {:?}", &xstate),
    };

    if ! xstate.remote_branch {
      let ThereIsNoMr = was_no_mr;
      let jpaths = self.xsas.generate_cve_json(xsa, &xstate)?;
      let new_commit = self.cvelist.make_xsajson_commit
        (&self.xsas, xsa, xstate.vsn, &jpaths)
        .context("make new cvelist commit")?;

      self.git_push(xsa, "push", &new_commit,
                    format!("{}:refs/heads/{}", &new_commit,
                            xsa.cvelist_branch_name()))
        .context("make remote cvelist branch")?;
    }

    let ThereIsNoMr = was_no_mr;

    let number = self.make_mr(xsa, &xstate)?;
    SO::MergeReq(number)
  }

  #[throws(AE)]
  fn git_push(&mut self, xsa: XsaNum,
              desc: &str, info: &dyn Debug,
              refspec: String) {
    self.cvelist.run_git(
      self.want_remote_actions(xsa, desc, info)?,
      desc,
      &["push"], "--quiet", Some("--dry-run"), &[
        self.cfg.cvelist_remote_xensec.to_string(),
        refspec.to_owned()
      ],
    )?;
  }

  #[throws(AE)]
  pub fn delete_branch(&mut self, xsa: XsaNum, global: &mut Global)
                       -> Option<String> {
    let xstate = global
      .cves.ascertain_one_xsa(self, &global.tagged, xsa,
                              // ascertain is mostly geared towards submission
                              // so we must encourate it to query the forge
                              true)?;
    let todo = xstate.todo(xsa, &global.cves);
    if ! matches!(todo, Some(analysis::Todo::DeleteBranch)) { return None }

    let branch = xsa.cvelist_branch_name();
    self.git_push(xsa, "cleanup", &branch,
                  format!(":refs/heads/{}", &branch))
      .with_context(|| branch.clone())
      .context("delete now-obsolete branch")?;
    Some(branch)
  }
}

impl NothingDone {
  #[throws(fmt::Error)]
  pub fn report(&self, w: &mut dyn fmt::Write, xsa: XsaNum) {
    use NothingDone::*;
    match self {
      ManualActionRequired(s) => {
        if s.len() > 0 {
          for s in s {
            writeln!(w, "{} NothingDone ManualActionRequired {}", xsa, s)?;
          }
        } else {
          writeln!(w, "{} NothingDone ManualActionRequired", xsa)?;
        }
      }
      MrOpen(number) => {
        writeln!(w, "{} MrOpen {:?}", xsa, number)?;
      }
      Embargoed | NoCves | OnlyForeignCves | Completed => {
        writeln!(w, "{} NothingDone {:?}", xsa, self)?;
      }
    }
  }
}
