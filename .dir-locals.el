; Copyright 2021 Citrix
; SPDX-License-Identifier: MIT OR Apache-2.0
; There is NO WARRANTY.

((rust-mode . ((rust-indent-offset . 2))))
